import {render, screen, cleanup} from '@testing-library/react'
import Main from '../main/main'
import Receiver from '../receiver/receiver'
import Sender from '../sender/sender'

test('Testing Main',()=>{
    render(<Main/>)
    const mainElement = screen.getByTestId('main');
    expect(mainElement).toBeInTheDocument();
})

test('Testing Sender',()=>{
    render(<Sender/>)
    const senderElement = screen.getByTestId('sender');
    expect(senderElement).toBeInTheDocument();
})

test('Testing Receiver',()=>{
    const primerResultado = 'primer resultado';
    const segundoResultado = 'segundo resultado';
    const tercerResultado = 'tercer resultado';
    const esPalindromo = Math.random() < 0.5; //Genero un random boolean para probar casos true y false de palindromos.
    
    render(<Receiver esPalindromo={esPalindromo} primerResultado={primerResultado} segundoResultado={segundoResultado} tercerResultado={tercerResultado}/>)
    const receiverElement = screen.getByTestId('receiver');
    const primerResultadoMostrado = screen.getByTestId('primerResultadoMostrado');
    const segundoResultadoMostrado = screen.getByTestId('segundoResultadoMostrado');
    const tercerResultadoMostrado = screen.getByTestId('tercerResultadoMostrado');
    let esPalindromoMostrado;
    if(esPalindromo) {esPalindromoMostrado = screen.getByTestId('esPalindromoMostrado')};
    
    //Controlo existencia de elementos
    expect(receiverElement).toBeInTheDocument();
    expect(primerResultadoMostrado).toBeInTheDocument();
    expect(segundoResultadoMostrado).toBeInTheDocument();
    expect(tercerResultadoMostrado).toBeInTheDocument();
    if(esPalindromo) {expect(esPalindromoMostrado).toBeInTheDocument()};

    //Controlo contenido de elementos
    expect(primerResultadoMostrado).toHaveValue('primer resultado');
    expect(segundoResultadoMostrado).toHaveValue('segundo resultado');
    expect(tercerResultadoMostrado).toHaveValue('tercer resultado');
    
})